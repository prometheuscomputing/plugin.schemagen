require 'spec_helper'
require 'fileutils'
require 'nokogiri'
require 'Foundation/load_path_management'
require 'indentation'

# NOTE: when debugging these tests, $output may be printed to see any errors that occurred during plugin execution via: puts $output

describe 'schemaGen' do
  before :all do
    @code_dir = File.expand_path('~/Prometheus/Generated_Code/')
    @schema_path = File.join(@code_dir, 'Test.xsd')
    # Remove it if it exists
    FileUtils.rm(@schema_path) if File.exist?(@schema_path)
  end
  
  context "Generating ElectionResults model as-is" do
    before :all do
      # Get the path to the schema
      
      # Run schemaGen on test.mdzip
      @message = MagicdrawPluginRunner.run(relative('../lib/schemaGen/schema_gen_runner.rb'), [relative('test.mdzip'), 'ElectionResults',
        '--schema_filename=Test.xsd',
        '--sort_other_attributes_following_base_attributes=true',
        '--sort_end_attributes_following_start_attributes=true',
        '--only_keep_compositions=true',
        '--beginning_comment=Beginning Comment'
      ])
      # Ensure that schema successfully generatedc
      expect(File.exist?(@schema_path)).to be true
      # Read the resulting XML Schema
      @schema_string = File.read(@schema_path)
      @schema_file = File.open(@schema_path)
      @doc = Nokogiri::XML(@schema_file) {|config| config.strict.noblanks}
      @schema_file.close
      @schema = @doc.children[1]
    end
  
    it "should not produce any errors" do
      expect(@message).not_to include('Error')
    end
    
    it "should not produce any warnings" do
      expect(@message).not_to include('Succeeded with warnings:')
    end
    
    it "should have a beginning comment" do
      bcomments = @doc.children.select{|c| c.comment? }
      expect(bcomments.length).to eq 1
      bcomment = bcomments.first
      expect(bcomment.text.strip).to eq 'Beginning Comment'
    end
  
    it "should have a root element named ElectionReport" do
      roots = @schema.children.select{|c| c.name == 'element' }
      # There can only be one.
      expect(roots.length).to eq 1
      root = roots.first
      expect(root['name']).to eq 'ElectionReport'
      expect(root['type']).to eq 'ElectionReport'
    end
    
    # Elements are sorted alphabetically
    # Attributes are sorted with ObjectId first, then alphabetically.
    # Attributes matching 'Other<attribute>' will be ordered after the matching attribute
    # Attributes matching 'Start/End<attribute>' will be ordered with End following Start
    it "should define attributes and elements in the specified order" do
      # Test element ordering
      candidate = @doc.at_xpath("//xsd:complexType[@name='Candidate']")
      elements = candidate.at_xpath("./xsd:sequence").children
      element_names = elements.collect{|e| e['name']}
      expect(element_names).to eq ["BallotName", "Code", "PartyId", "PersonId"]
      # Test normal attribute ordering
      attributes = candidate.children.select{|c| c.name == 'attribute'}
      attribute_names = attributes.collect{|a| a['name']}
      expect(attribute_names).to eq ["ObjectId", "FileDate", "Id", "IsIncumbent", "IsTopTicket", "PostElectionStatus", "PreElectionStatus", "SequenceOrder"]
      # Test normal attribute ordering (example 2)
      election_report = @doc.at_xpath("//xsd:complexType[@name='ElectionReport']")
      attributes = election_report.children.select{|c| c.name == 'attribute'}
      attribute_names = attributes.collect{|a| a['name']}
      expect(attribute_names).to eq ["Format", "GeneratedDate", "Issuer", "IssuerAbbreviation", "IsTest", "Sequence", "SequenceEnd", "Status", "TestType", "VendorApplicationId"]
      # Test Type/OtherType attribute ordering
      count_status = @doc.at_xpath("//xsd:complexType[@name='CountStatus']")
      attributes = count_status.children.select{|c| c.name == 'attribute'}
      attribute_names = attributes.collect{|e| e['name']}
      expect(attribute_names).to eq ["Status", "Type", "OtherType"]
      # Test Type/OtherType element ordering (example 2)
      contest = @doc.at_xpath("//xsd:complexType[@name='Contest']")
      attributes = contest.children.select{|c| c.name == 'attribute'}
      attribute_names = attributes.collect{|e| e['name']}
      expect(attribute_names).to eq ["ObjectId", "Abbreviation", "HasRotation", "Name", "SequenceOrder", "SubUnitsReported", "TotalSubUnits", "VoteVariationType", "OtherVoteVariationType"]
      # Test Start/End element ordering
      hours = @doc.at_xpath("//xsd:complexType[@name='Hours']")
      attributes = hours.children.select{|c| c.name == 'attribute'}
      attribute_names = attributes.collect{|e| e['name']}
      expect(attribute_names).to eq ["Day", "StartTime", "EndTime"]
    end
    
    # XML properties are sorted with name, type, minOccurs, maxOccurs, and use appearing first, then alphabetically
    it "should define XML properties according to the specified ordering" do
      xml_attributes = @doc.at_xpath("//xsd:complexType[@name='Candidate']/xsd:sequence/xsd:element[@name='Code']").attribute_nodes
      xml_attribute_names = xml_attributes.collect{|xa| xa.name}
      expect(xml_attribute_names).to eq ["name", "type", "minOccurs", "maxOccurs"]
    end
    
    it "should generate attributes normally" do
      # TODO: this should be converted to use Nokogiri
      expect(@schema_string).to include('<xsd:attribute name="HasRotation" type="xsd:boolean"/>')
      # Check classes with a <<simpleContent>> attribute
      expect(@schema_string).to include <<-EOS.chomp.reset_indentation(2)
        <xsd:complexType name="LanguageString">
          <xsd:simpleContent>
            <xsd:extension base="xsd:string">
              <xsd:attribute name="Language" type="xsd:language" use="required"/>
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      EOS
      # Check ObjectId
      expect(@schema_string).to include('<xsd:attribute name="ObjectId" type="xsd:ID" use="required"/>')
    end
  end
  
  context "Generate with custom options" do
  
    it "should work with the use_collection_containers option" do
      test_schema_filename = 'TestCollections.xsd'
      test_schema = File.join(@code_dir, test_schema_filename)
      FileUtils.rm(test_schema) if File.exist?(test_schema)
      @message = MagicdrawPluginRunner.run(relative('../lib/schemaGen/schema_gen_runner.rb'), [relative('test.mdzip'), 'ElectionResults', 
        "--schema_filename=#{test_schema_filename}",
        '--use_collection_containers=true'])
      expect(@message).not_to include('Error')
      expect(@message).not_to include('Succeeded with warnings:')
      expect(File.exist?(test_schema)).to be true
      # TODO: this should be converted to use Nokogiri
      schema_string = File.read(test_schema)
      expect(schema_string).to include('VoteCountsCollection')
    end
  
    it "should work with the generate_attributes_as_elements option" do
      test_schema_filename = 'TestGenAttrsAsElements.xsd'
      test_schema = File.join(@code_dir, test_schema_filename)
      FileUtils.rm(test_schema) if File.exist?(test_schema)
      @message = MagicdrawPluginRunner.run(relative('../lib/schemaGen/schema_gen_runner.rb'), [relative('test.mdzip'), 'ElectionResults', 
        "--schema_filename=#{test_schema_filename}",
        '--generate_attributes_as_elements=true'])
      expect(@message).not_to include('Error')
      expect(@message).not_to include('Succeeded with warnings:')
      expect(File.exist?(test_schema)).to be true
      # TODO: this should be converted to use Nokogiri
      # Check that normal attributes are generated as elements
      schema_string = File.read(test_schema)
      # expect(schema_string).to include('<xsd:element minOccurs="0" name="HasRotation" type="xsd:boolean"/>')
      # Check that classes with a <<simpleContent>> attribute are not affected (since this would be invalid)
      expect(schema_string).to include <<-EOS.chomp.reset_indentation(2)
        <xsd:complexType name="LanguageString">
          <xsd:simpleContent>
            <xsd:extension base="xsd:string">
              <xsd:attribute name="Language" type="xsd:language" use="required"/>
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      EOS
      # Check that ObjectId is unaffected
      expect(schema_string).to include('<xsd:attribute name="ObjectId" type="xsd:ID" use="required"/>')
    end
  end
end