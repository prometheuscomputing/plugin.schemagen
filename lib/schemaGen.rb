
# Build an XML schema for the model elements contained in the selected package
#RubyAction.new("Generate Java Objects",   :SchemaGen, Package, Model) {action :generate_javaSchema}
RubyAction.new(:schemaGen, "Generate XML Schema",   :SchemaGen, Package, Model) {action :generate_xmlSchema}

# Actions that belong elsewhere:
# RubyAction.new(:schemaGen, "Generate Reference Implementation",   :SchemaGen, Package, Model) {action :generate_rubySequel}
# RubyDiagramAction.new(:schemaGen, "Generate Reference Implementation", :SchemaGen, "Any Diagram") {action :generate_rubySequel_from_diagram}


# This action can be tested by application to the Data package in
# /Users/griesser/Documents/Real/Projects_Current/DIM/DIM2/DIM6a.mdxml
# This generates one schema per sub package.
# (need to dig up the original test model that had vulture, cenotaph, etc.)