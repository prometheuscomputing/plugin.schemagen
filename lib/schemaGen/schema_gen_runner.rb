# Put the project on the load path
require 'Foundation/load_path_management'
$:.unshift relative('../')

# Put local dependencies on the load path (Checks for sibling project folders)
$:.unshift relative('../../../magicdraw_plugin_runner/lib/')
$:.unshift relative('../../../magicdraw_extensions/lib/')
$:.unshift relative('../../../modelDriven_builder/lib/')

require 'magicdraw_plugin_runner/plugin_runner'

# Load magicdraw_extensions
# Only magic_draw_stereotypes is used by this file, but it seems to mess up the "load 'magicdraw_extensions.rb'" call later,
# so other magicdraw_extensions are required here as well
#require 'magicdraw_extensions' # <- Seems to be broken
require 'magicdraw_extensions/object_extensions'
require 'magicdraw_extensions/magic_draw_stereotypes'
require 'magicdraw_extensions/magic_draw_nav'

class SchemaGenRunner < MagicdrawPluginRunner::PluginRunner
  def setup_for_project_load
    # Example of how to setup default project path or arguments
    #self.project_path ||= relative('test.mdzip')
    #self.args[0] ||= 'Test'
  end
  
  def activate_plugin(md_env = {})
    app = md_env[:app]
    project = md_env[:project]
    package = args[0]
    
    # Get the specified package
    model = project.getModel
    nested_packages = model.getNestedPackage
    md_package = nested_packages.find{|p| p.getName == package}
    raise "Couldn't find package #{package}!" unless md_package
    
    # Set any forced schema generation options
    schema_stereotype = md_package.get_stereotype('XML Schema')
    set_stereotype_options(args, project, md_package, schema_stereotype)
    
    # Select md_package in containment tree
    browser = app.getMainFrame.getBrowser
    containment_tree = browser.getContainmentTree
    containment_tree.openNode(md_package, select=true, appendSelection=false, requestFocus=true, scrollToVisible=false)
    
    # How to navigate and use nodes:
    # project_node = containment_tree.getRootNode
    # data_node = project_node.children.first
    # md_package_node = data_node.children.find{|c| c.getUserObject.getName == package}
    
    # How to export diagrams
    # diagrams = app.getProject.getDiagrams
    # diagrams.each do |d|
    #   System.out.println "d.inspect: #{d.inspect}"
    #   System.out.println "Found diagram: #{d.getHumanName}"
    #   System.out.println " with id: #{d.getID}"
    #   diagramFile = java.io.File.new(File.join('/Users/sdana/tmp', d.getHumanName + d.getID + '.png'))
    #   ImageExporter.export(d, ImageExporter::PNG, diagramFile)
    # end
    
    # Trigger the plugin action
    # actions_menu_creator = com.nomagic.magicdraw.actions.MenuCreatorFactory.getMenuCreator
    ap = com.nomagic.magicdraw.actions.ActionsProvider.getInstance
    actions_manager = ap.getContainmentBrowserContextActions(containment_tree)
    actions = actions_manager.getAllActions
    generate_xml_action = actions.find{|a| a.getName == "Generate XML Schema"}
    raise "Could not find context action 'Generate XML Schema'!" unless generate_xml_action
    generate_xml_action.actionPerformed(nil)
  end
  
  def set_stereotype_options(arguments, project, package, stereotype)
    arguments.each do |arg|
      if arg =~ /--(.+)=(.+)/
        tag = $1
        value = case $2
        when 'false'
          type = 'boolean'
          false
        when 'true'
          type = 'boolean'
          true
        else
          type = 'String'
          $2
        end
        slot = StereotypesHelper.getSlot(package, stereotype, tag, createSlotIfNotExists=true)
        # If slot isn't present, create an appropriate tag definition on stereotype
        # This is a convenience feature so new generation options don't always need to result in a change to the test model. -SD
        unless slot
          factory = project.getElementsFactory
          tag_def = factory.createPropertyInstance
          tag_def.setName(tag)
          tag_def.setDerived(false)
          tag_def.setUnique(false)
          tag_def.setOwner(stereotype)
          tag_type = ModelHelper.findDataTypeFor(project, tag, nil)
          tag_def.setType(tag_type)
          slot = StereotypesHelper.getSlot(package, stereotype, tag, createSlotIfNotExists=true)
        end
        StereotypesHelper.setSlotValue(slot, value, appendValues=false, setAnotherEnd=false)
      end
    end
  end
end

$plugin_runner = SchemaGenRunner