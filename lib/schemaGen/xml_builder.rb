# NOTE:  if reproduceable_builder, xml_builder, and uml_models are switched from 'require' to 'load' a stack overflow occurs on the second execution of xml schema generation.
#        The resulting stack dump is inconsistent, often does not show much depth, and tends to occur in innocuous code.
#        I've not investigated which of these files is actually responsible, or why they behave this way.
# require 'modelDriven_builder/reproduceable_builder'
require 'modelDriven_builder/edmonds_filter'

require 'magicdraw_extensions/treeDump'

class Extension
  attr_accessor :extended
  attr_accessor :extenders
  def initialize(extended)
    self.extended = extended
    self.extenders = Array.new
  end
  def getName; extended.getName; end
  def <=>(other); getName <=> other.getName; end
end

# THIS CODE REQUIRED FOR OLD modelDriven_builder CODE TO WORK
require 'fileutils'
include FileUtils
# =============================================================================
# =============================================================================
#           THIS CODE COPIED FROM OLD VERSION OF modelDriven_builder
# =============================================================================
# =============================================================================
require 'builder'
require 'singleton'
require 'indentation'

# This modifies builder so that XML attributes are written out in alphabetical order.
# Of course the XML does not care, but the sorting makes it possible to compare
# today's output with last months (for regression testing).

class Symbol
  def <=>(arg); to_s<=>arg.to_s; end
end

class String
  alias broken_compare <=>
  def <=>(arg); broken_compare(arg.to_s); end
end
  

# Subclass of Builder's XmlMarkup class that sorts xml attributes
# Also provides #indented_text! and #indented_multiline_text! to allow indented text blocks
class XmlMarkupWithSortedAttrs < Builder::XmlMarkup
  def _insert_attributes(attrs, order=[])
    # For now, hardcode NIST desired formatting.
    # TODO: replace this with stereotype option
    if attrs && order.empty?
      # Set attributes to always appear first
      order = [:name, :type, :minOccurs, :maxOccurs, :use]
      # Set all other attributes to appear alphabetically
      order += (attrs.keys.sort - order)
    end
    super(attrs, order)
  end
  
  # Unlike text!, this method inserts the given text at the current indent level
  def indented_text!(text)
    _indent
    text!(text)
  end
  
  # Indents a given multiline string, postfixing a newline
  def indented_multiline_text!(text)
    text!(text.indent(@level * @indent) + "\n")
  end
  
  def indented_multiline_text_unescaped!(text)
    self << (text.indent(@level * @indent) + "\n")
  end
  
  #Escaped text with additional whitespace stripped
  def escaped_stripped_text!(text)
    self << _escape(text).strip
  end
end

# Previously called DDLGenerator, but used for many purposes other than DDL generation.
# Wraps a builder with logging and knowlege of directory.
# Note that the builder used here is *not* any subclass of ModelDriven_Builder. It's a lower-level builder
#    such as SpecBuilder (from plugin.specGen), RubyCodeBuilder (from plugin.rubyGen), 
#    RubyBuilder (from plugin.rubyGen), JavaBuilder (from plugin.rubyGen),  etc.
# Wrapping a ModelDriven_Builder like this is not a great idea, because higher level code talks to the
#    director, not the builder.
# SHOULD NOT BE A SINGLETON???
class Generator
  include Singleton
  attr_accessor :builder
  attr_accessor :package
  attr_accessor :file_root
  
  def warnings; Warnings.list; end
  
  def self.warnings; Warnings.list; end
  def self.builder; instance.builder; end
  def self.file_root; instance.file_root; end
  
  def initialize
    Warnings.clear
    self.file_root = File.join(Prometheus, 'Generated_Code')
    mkdir_p(file_root) unless File.exists?(file_root)
    raise "#{file_root} already exists, but is not a directory" unless File.directory?(file_root)
  end
  
  def clear
    @builder = nil
    @package = nil
    @main_module_name = nil
    Warnings.clear
  end
  
  def build
    raise "Invalid Generator setup" unless builder && package
    output = builder.build(package)
    puts
    #puts output
    if warnings.empty?
      message = 'Success, no warnings!'
    else # Warnings exist
      message = "Succeeded with warnings:\n"
      message += warnings.join("\n")
    end
    puts "#{builder.class.to_s}: #{message}"
    message
  end

  def main_module_name
    raise "fooey"
    raise "Can't determine #main_module_name because @package is nil" unless @package
    if @main_module_name
      @main_module_name
    elsif $MAIN_MODULE
      # Not clear why this possibility was encoded in SpecGenerator, it does not seem to be used
      @main_module_name = $MAIN_MODULE
    else
      root_package = get_root_package
      # Attempt to get main module name from options
      options = root_package.get_tag_values_hash('options', :fail_silently) if root_package
      project_name_from_options = options['project_name'].first if options['project_name']
      unless project_name_from_options
        # We are asking for confirmation of the project name if there was no project name in options
        project_name = capitalize_first_letter(root_package.getName)
        # Ensure capitalization of project name
        
        @main_module_name = Java::javax.swing.JOptionPane.showInputDialog("Package name to generate into", project_name) + "_Generated"
      else
        @main_module_name = project_name_from_options + "_Generated"
      end
    end
  end

  def project_version
    raise "Can't determine #project_version because @package is nil" unless @package
    if @project_version
      @project_version
    else
      root_package = get_root_package
       # Attempt to get version from options
      # options = root_package.get_tag_values_hash('options', :fail_silently) if root_package
      @project_version = root_package.get_tag_value(:options, :version) if root_package
      @project_version = '0.0.0' unless @project_version && @project_version.is_a?(String) && !@project_version.empty?
    end
    @project_version
  end
  
  def get_root_package
    if self.package.is_a?(Enumerable)
      if self.package.size > 1
        # If there are multiple packages we are going to look in the owner of the first package and see if there is any metadata there that helps us out.  This is not foolproof because there is no guarantee that all of the selected packages have a common owner.
        self.package.first.getOwner
      else
        self.package.first
      end
    else
      # And this shouldn't ever happen if $SELECTIONS always returns an array...which it is.  Maybe this is useful if this code also handles $SELECTION
      self.package
    end
  end

  
end
# =============================================================================
# =============================================================================
#           END OF CODE COPIED FROM OLD VERSION OF modelDriven_builder
# =============================================================================
# =============================================================================

class XMLBuilder
  include Edmonds_Filter

  attr_reader :xml_builder
  attr_reader :xml
  attr_accessor :model_package
  attr_accessor :schema_attributes
  attr_accessor :use_collection_containers
  # Whether or not to rely only on associations' composition status to determine whether or not they are 'kept'
  # Implies always_keep_compositions=true
  attr_accessor :only_keep_compositions
  # Whether or not compositions are always 'kept' as definitions and not references
  attr_accessor :always_keep_compositions
  attr_accessor :object_id_only_if_referenced
  # Whether or not to append 'ID' (or other specified string) to reference element names
  attr_accessor :append_id_to_reference_names
  # Specifies the string to append to reference names. Defaults to 'ID'
  attr_accessor :id_suffix_for_references
  attr_accessor :enforce_case_conventions
  attr_accessor :id_attribute_name
  attr_accessor :uppercase_attribute_names
  # Whether or not to append 'Type' to all type names
  attr_accessor :append_type_to_type_names
  # Whether or not to include UML documentation as XML annotations
  attr_accessor :include_documentation_annotations
  # Whether documentation should appear inline or on a new line
  attr_accessor :documentation_inline
  # Full version number of the generated schema (e.g. 1.0)
  attr_accessor :full_version
  # Major version number of the generated schema (e.g. 1)
  attr_accessor :major_version
  # Filename of the generated schema
  attr_accessor :schema_filename
  # Whether or not to use xsd:IDREFS for referenced elements that have multiplicity
  # Additionally adds an 's' to the element's name if append_id_to_reference_names is true
  attr_accessor :use_idrefs
  # Used if use_idrefs is true. Defaults to adding 'S' to id_suffix_for_references
  attr_accessor :id_suffix_for_plural_references
  # Whether or not to make the element used to contain group references optional (provided zero references is allowed)
  attr_accessor :optional_multiplicity_on_group_element
  # Whether or not to generate UML attributes as XML elements
  # If true, attributes are generated as XML elements unless <<xmlAttribute>> is defined on the attribute.
  # If false, attributes are generated as XML attributes unless <<xmlElement>> is defined on the attribute.
  attr_accessor :generate_attributes_as_elements
  # Whether or not to sort attributes matching "Other<attribute>" to follow the matched <attribute>.
  # If false, normal alphabetical ordering applies
  # Defaults to false
  attr_accessor :sort_other_attributes_following_base_attributes
  # Whether or not to sort 'End*' or '*End' attributes after matching 'Start' attributes
  # Defaults to false
  attr_accessor :sort_end_attributes_following_start_attributes
  # A comment to insert just following the <?xml...> declaration
  attr_accessor :beginning_comment
  # Whether or not to indent documentation content
  attr_accessor :indent_documentation
  
  def self.generate_schema(model)
    # TODO: implement model driven builder use seen here:
    # xml_builder = self.new(model)
    # director = ModelDriven_Director.new(xml_builder, false, false)
    # director.process_model(model)
    
    
    generator = Generator.instance
    generator.package = $SELECTION
    schemaGen_action = $Action
    
    builder = XMLBuilder.new
    
    # folder_name = generator.main_module_name.downcase
    # generation_folder = File.join(Generator.file_root, folder_name)
    # puts "Generation folder: #{generation_folder}"
    # puts '?' * 50
    results = []
    
    # Generate the reference implementation (run schemaGen)
    generator.builder = builder
    generator_result = generator.build
    results << generator_result unless generator_result =~ /Success, no warnings!/
    $MESSAGE = results.empty? ? "Success, no warnings!" : results.join("\n")
  end
  
  def setup(model_package)
    # self.schema_attributes = { :elementFormDefault=>"qualified", 'xmlns:xsd' => "http://www.w3.org/2001/XMLSchema" }
    self.schema_attributes = { 'xmlns:xsd' => "http://www.w3.org/2001/XMLSchema", "elementFormDefault" => "qualified" }
    buffer = String.new
    @xml = buffer
    @xml_builder = XmlMarkupWithSortedAttrs.new(:target=>buffer, :indent=>2)
    self.model_package = model_package
    @use_collection_containers = model_package.package.get_tag_value('XML Schema', 'use_collection_containers') || false
    @only_keep_compositions = model_package.package.get_tag_value('XML Schema', 'only_keep_compositions') || false
    @always_keep_compositions = @only_keep_compositions || model_package.package.get_tag_value('XML Schema', 'always_keep_compositions') || false
    @object_id_only_if_referenced = model_package.package.get_tag_value('XML Schema', 'object_id_only_if_referenced') || false
    @append_id_to_reference_names = model_package.package.get_tag_value('XML Schema', 'append_id_to_reference_names') || false
    @id_suffix_for_references = model_package.package.get_tag_value('XML Schema', 'id_suffix_for_references') || 'ID'
    @use_idrefs = model_package.package.get_tag_value('XML Schema', 'use_idrefs') || false
    @id_suffix_for_plural_references = model_package.package.get_tag_value('XML Schema', 'id_suffix_for_plural_references') || (@id_suffix_for_references + 'S')
    @enforce_case_conventions = model_package.package.get_tag_value('XML Schema', 'enforce_case_conventions') || false
    @id_attribute_name = model_package.package.get_tag_value('XML Schema', 'id_attribute_name') || 'object_id'
    @uppercase_attribute_names = model_package.package.get_tag_value('XML Schema', 'uppercase_attribute_names') || false
    @append_type_to_type_names = model_package.package.get_tag_value('XML Schema', 'append_type_to_type_names') || false
    @include_documentation_annotations = model_package.package.get_tag_value('XML Schema', 'include_documentation_annotations') || false
    @documentation_inline = model_package.package.get_tag_value('XML Schema', 'documentation_inline') || false
    @full_version = model_package.package.get_tag_value('XML Schema', 'full_version') || '1.0'
    @major_version = model_package.package.get_tag_value('XML Schema', 'major_version') || '1'
    @schema_filename = model_package.package.get_tag_value('XML Schema', 'schema_filename') || ''
    # Note: This option defaults to true, which will produce a different schema than previous versions of schemaGen.
    #       However, the generated schemas will be more permissive than previously generated ones, so any instances should still validate with the new schema.
    @optional_multiplicity_on_group_element = model_package.package.get_tag_value('XML Schema', 'optional_multiplicity_on_group_element')
    @optional_multiplicity_on_group_element = true if @optional_multiplicity_on_group_element.nil?
    @generate_attributes_as_elements = model_package.package.get_tag_value('XML Schema', 'generate_attributes_as_elements') || false
    @sort_other_attributes_following_base_attributes = model_package.package.get_tag_value('XML Schema', 'sort_other_attributes_following_base_attributes') || false
    @sort_end_attributes_following_start_attributes = model_package.package.get_tag_value('XML Schema', 'sort_end_attributes_following_start_attributes') || false
    @beginning_comment = model_package.package.get_tag_value('XML Schema', 'beginning_comment')
    @indent_documentation = model_package.package.get_tag_value('XML Schema', 'indent_documentation') || false
    
    # Perform any variable substitutions (e.g. <MAJOR_VERSION> in namespace)
    @schema_filename = substitute_variables(@schema_filename)
  end
  
  def substitute_variables(option)
    variables = {'MAJOR_VERSION' => @major_version, 'FULL_VERSION' => @full_version}
    variables.each do |name, value|
      option = option.gsub("<#{name}>", value)
    end
    option
  end

  # === Additional helpers ===
  alias b xml_builder
  def is_target?(uri)
    @model_package.is_target?(uri)
  end

  def is_available?(uri)
    @model_package.is_available?(uri)
  end
  
  # __Qname is broken. It only inserts the namespace's prefix if the attribute's package's namespace is not equal to itself.
  # I don't want to fix __Qname until I know how it was supposed to work, so I'm writing this schemaGen specific method. -SD
  def qualified_name(type)
    name = type.getName
    pkg = type.getPackage
    if pkg
      prefix, uri = pkg.__Namespace
      # Invoking schemaGen method is_target? here, not to be confused w/ magicdraw_extensions' version.
      name = "#{prefix}:#{name}" unless is_target?(uri)
    end
    name
  end 
  
  # === Build Fragments ===
  def build_package(pkg)
        # A package that contains others is just an organizer, not a schema.
        return pkg.packages.sort_by{|p|p.getName}.each {|pkg| build_package(pkg) } unless pkg.packages.empty?
        # We have a package that represents a schema
        #puts "Writing schema for package #{pkg.getName} to #{schema_filePath(pkg)}"
        self.model_package = UMLPackage.new(pkg)
        analyze_graph(pkg)
        prefix, uri = model_package.target_namespace
        attributes = schema_attributes.clone
        attributes[:xmlns]=substitute_variables(uri)
        attributes[:targetNamespace]=substitute_variables(uri)
        attributes[:version]=@full_version unless @full_version.empty?
        model_package.imported_namespaces.each {|ns|
          attributes['xmlns:'+ns[0]]=ns[1]
          }
        start_index = xml.size
        b.instruct! :xml, :version=>"1.0", :encoding=>"UTF-8"
        b.comment! @beginning_comment if @beginning_comment && !@beginning_comment.empty?
        b.xsd(:schema, attributes) {
          b.comment! '========== Imports ==========' if pkg.imported_packages.any?
          sorted_imports = pkg.imported_packages.sort_by{|imp| imp.__Namespace[1] }
          sorted_imports.each {|imp|
            # For some reason imp.get_uri returns empty string here! No time to debug currently. -SD
            namespace = imp.__Namespace[1]
            schema_location = imp.get_tag_value('XML Schema', 'schemaLocation') || namespace
            b.xsd(:import, :namespace=>namespace, :schemaLocation=>schema_location)
            }
          b.comment! '========== Roots ==========' if pkg.roots.any?
          pkg.roots.sort.each      {|c| build_root(c)}
          b.comment! '========== Primitives ==========' if pkg.primitives.any?
          pkg.primitives.sort.each   {|p| build_primitive(p) }
          b.comment! '========== Enumerations ==========' if pkg.enumerations.any?
          pkg.enumerations.sort.each {|e| build_enumeration(e) }
          b.comment! '========== Interfaces Defined ==========' if pkg.interfaces.any?
          pkg.interfaces.sort.each   {|i| build_interface(i) }
          build_extensions(pkg)
          b.comment! '========== Classes ==========' if pkg.classes.any?
          pkg.classes.sort.each      {|c| build_class(c, pkg) }
        }
        File.open(schema_filePath(pkg), 'w+') {|file| file << xml[start_index..-1] }
        b.comment! '==========================================================='
  end

  def build_extensions(pkg)
    extensions = Hash.new
    pkg.classes.each {|cls|
      cls.__interfaces_implemented.each {|interface| 
        # puts "Pkg: #{pkg.getName.inspect}  ignore? #{is_target?(interface.get_uri)}  Class.getName: #{cls.getName.inspect}  Interface.getName: #{interface.getName.inspect}  Interface.get_uri: #{interface.get_uri.inspect}   builder.target_namespace: #{target_namespace.inspect}"
        next if is_target?(interface.get_uri) 
        unless interface.__is_extendable?
          # We currently get an annoying warning that when insde AmustementPark and we come to the Attraction Interface that's implemented *IN THE MODEL* by Zoo.
          # To prevent this, instead of the above "next if is_target?(interface.get_uri)" we should additionally move on to the next inteface if the
          # interface is NOT imported into pkg.
          Warnings << "Ignored attempt to extend non-extendable interface #{interface.getName} by #{cls.getName}"
          next
        end
        extension = extensions[interface.getID]||=Extension.new(interface)
        extension.extenders << cls
        }
      }
    b.comment! '========== Interfaces Extended ==========' if extensions.values.any?
    extensions.values.sort.each {|extension| 
      classes = extension.extenders
      classes.sort.each {|cls|
        b.xsd(:element, :name=>cls.getName, :substitutionGroup=>qualified_name(extension.extended), :type=>qualified_name(cls))
        }
      }
  end
  def build_primitive(prim)
      # This code does not change when we switch from 
      #    Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PrimitiveType
      # to classes that have <<prim>> stereotype.
      type_name = prim.getName
      type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
      insert_xsd_with_documentation(prim, :simpleType, :name => type_name) {
        b.xsd(:restriction, :base=>xml_super(prim) ) {
          # Add any attributes of the primitive as elements in the restriction
          # Currently requires that attributes have a default value that is a string type.
          # MD type is: com.nomagic.uml2.ext.magicdraw.classes.mdkernel.impl.LiteralString(Impl)
          sorted_attributes(prim).each {|facet|
            raise "No default value defined for #{facet.getName} restriction on #{type_name}" unless facet.getDefaultValue
            raise "Unable to get default value for #{facet.getName} restriction on #{prim.getName}: #{facet.getDefaultValue.inspect}" unless facet.getDefaultValue.respond_to?(:getValue)
            insert_xsd_with_documentation(facet, facet.getName.to_sym, :value=>facet.getDefaultValue.getValue)
          }
        }
      }
  end
  def build_enumeration(enum)
      type_name = enum.getName
      type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
      insert_xsd_with_documentation(enum, :simpleType, :name => type_name) {
        b.xsd(:restriction, :base=>xml_super(enum) ) {
          enum.literals.each {|literal| 
            insert_xsd_with_documentation(literal, :enumeration, :value => literal.getName)
          }
        }
      }
  end
  def build_interface(interface)
      # Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::InterfaceRealization
      name = interface.getName
        b.comment! "=== Interface #{name} ==="
#puts "Interface #{name}"
      if interface.__is_extendable?
#puts "\tIS EXTENDABLE"
        # Substitution groups do not let a given class implement multiple interfaces.
        insert_xsd_with_documentation(interface, :element, :name=>name, :abstract=>'true', :type=>'xsd:anyType', :abstract=>'true')
        # InterfaceRealization instances sort on the basis of the implementing classes (for reproducability).
        realizations = interface.get_interfaceRealizationOfContract.to_array.sort
        realizations.sort.each {|realization|
          classifier = realization.getImplementingClassifier
#puts "\t#{classifier.getName} available: #{is_available?(classifier.get_uri)}"
          next unless is_available?(classifier.get_uri)
          # E18 PVType insists on having multiple choices for the same type (for example xsd:int can have an element name of either I4 or EI). 
          # The element name is adding some additonal semantics to data (this should probably be done with a subclass xsd:int instead)
          # Nevertheless, we are stuck with it for the time being. It will probably mess up the import and the Pippin unmarshalling.
          eNames = (realization.get_tag_value(:Element, :names) || classifier.getName).split(',').collect {|_name| _name.strip}
          eNames.sort.each {|eName|
            b.xsd(:element, :name=>eName, :substitutionGroup=>name, :type=>qualified_name(classifier))
            }
          }
#puts "\t======================"
      else
        insert_xsd_with_documentation(interface, :group, :name=>name) { b.xsd(:choice) {_build_implementers(interface)} }
      end
  end
  def build_class(cls, pkg)
      # Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class
      superClasses = cls.getSuperClass
      
      # Get the superclass
      sc = case
      when superClasses.size < 1
        nil
      when superClasses.size == 1
        get_xsd_type_for_type(superClasses[0]) || qualified_name(superClasses[0])
      else # superClasses.size > 1
        name = superClasses[0].getName
        Warnings << "Class #{cls.getName} has multiple superClasses: presuming #{name}"
        get_xsd_type_for_type(superClasses[0]) || qualified_name(superClasses[0])
      end
      
      # Look for simpleContent attributes
      # These indicate that the class should extend the base type of the attribute
      # in a simpleContent block.
      simple_content_attrs = cls.getOwnedAttribute.select{|a| a.get_stereotype('simpleContent')}
      # There can only be one
      raise "Error: Found more than one simpleContent attribute on class #{cls.getName}" if simple_content_attrs.length > 1
      simple_content_attr = simple_content_attrs.first
      
      type_name = cls.getName
      type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
      attrs = {:name=>type_name}
      attrs[:abstract]='true' if cls.isAbstract
      insert_xsd_with_documentation(cls, :complexType, attrs) {  # Substitution groups are (necessarily) handled at the element level
        case
        when simple_content_attr
          simple_content_base_type = simple_content_attr.getType
          raise "Error: simpleContent attribute must have a type." unless simple_content_base_type
          raise "Error: simpleContent attribute must be a primitive type." unless simple_content_base_type.primitive?
          simple_content_base_type_name = get_xsd_type_for_attribute(simple_content_attr) || qualified_name(simple_content_base_type)
          b.xsd(:simpleContent) {
            b.xsd(:extension, :base => simple_content_base_type_name) { _build_content_for_cls(cls, pkg) }
          }
        when sc
          b.xsd(:complexContent) {
            sc = sc + 'Type' if @append_type_to_type_names && !(sc =~ /^xsd:/)
            b.xsd(:extension, :base=>sc) { _build_content_for_cls(cls, pkg) }
          }
        else
          _build_content_for_cls(cls, pkg, true)
        end
      }
  end
  def build_root(cls)
    return unless cls.get_stereotype('Root')
    type_name = cls.getName
    type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
    name = cls.get_tag_value(:Root, :name) || cls.getName
    b.xsd(:element, :name=>name, :type=>type_name)
  end
  def _build_content_for_cls(cls, pkg, include_id=false)
    cls_is_assoc_class = cls.isAssociationClass?
    # NOTE: cls.has_association? is insufficient becuase it doesn't check for primitive attributes with multiplicities greater than 1.
    #       However, I don't want to change it, since I don't know what else is using it.
    #       Additionally, I want to run _build_associations_for_cls if the xmlElement stereotype is used. -SD
    cls_has_associations = cls.getOwnedAttribute.any?{|a|
      type = a.getType
      next false unless type
      # class has associations if this attribute has a class or interface type
      type.class? || type.interface? ||
      # class has associations if this attribute is tagged with the <<xmlElement>> stereotype
      a.get_stereotype('xmlElement') ||
      # class has associations if this attribute is a primitive or enumeration that has a multiplicity greater than 1
      ((type.primitive? || type.enumeration?) && a.getUpper != 1) ||
      # class has associations if this attribute is a primitive or enumeration that is going to be generated as an element 
      # due to @generate_attributes_as_elements being set to true (unless this attribute is also tagged with the <<xmlAttribute>> stereotype).
      ((type.primitive? || type.enumeration?) && @generate_attributes_as_elements && a.get_stereotype('xmlAttribute').nil?)
    }
    # classes that have a <<simpleContent>> attribute are not allowed to have associations (or be an association class)
    cls_has_simple_content = cls.getOwnedAttribute.any?{|a| a.get_stereotype('simpleContent')}
    if cls_has_simple_content && (cls_has_associations || cls_is_assoc_class)
      # NOTE: Not printing a warning here since simpleContent classes may have non-navigable associations to the elements that contain them.
      #       Currently, this is not taken into account by cls_has_associations, and should be fixed -SD
      puts "Class #{cls.name} has both a <<simpleContent>> attribute and associations which are incompatible. The associations will be ignored."
      cls_has_associations = false
      cls_is_assoc_class = false
    end
    b.xsd(:sequence) { _build_associations_for_cls(cls) } if (cls_has_associations || cls_is_assoc_class)
    # Add qualifiers as attributes, rather than creating some kind of dictionary structure - if necessary, a dictionary can be constructed in the marshalling/unmarshalling API
    qualifiers = Array.new
    cls.pertinent_qualifiers(pkg).each {|qs|
      (Warnings << "Found association with #{qs.size} qualifiers, #{qs.collect {|q| q.getName}.inspect}, ignoring all but first") if qs.size>1
      qualifiers << qs[0]
      }
    qualifiers = qualifiers.sort {|a, b| a.getName <=> b.getName}
    # It would be nice to support a uniqueness or key constraint on qualifiers. Unfortunately, XML Schema defines these only for elements.
    # We use elements to represent roles, rather than types.  Applying qualifiers to specific roles might be appropriate in some contexts,
    # but most of the time they are not role dependent.  Additionally, role dependent uniquess contraints are difficult to apply to SQL schemas.
    # We could add the constraints to role-elements anyway (since qualifiers are available when elements are defined).  It's probably better to use xsd::ID
    # in the model. This is complicated by XML Schemas insistence that a complex type have only one attribute derived from xsd::ID.
    id = qualifiers.detect {|qualifier| 'xsd:ID'==qualified_name(qualifier.getType)}
    # If object_id_only_if_referenced is set to true, exclude the object_id if this class is not 'referenced'
    if object_id_only_if_referenced
      navigable_assocs = cls.getAllAssociationAttributes.select{|a| a.inverse_navigable?}
      # The schema generator will always count assign one association as the container for the
      # associated item. If there is only one association, then there are no 'reference' associations.
      if navigable_assocs.count <= 1
        is_referenced = false
      elsif always_keep_compositions
        navigable_assocs = navigable_assocs.reject{|a| a.inverse_composite?}
        is_referenced = navigable_assocs.any?
      else
        is_referenced = true
      end
      exclude_id = !is_referenced
    else
      exclude_id = false
    end
    b.xsd(:attribute, :name=>@id_attribute_name, :type=>'xsd:ID', :use=>'required') if include_id && !id && !exclude_id
    qualifiers.each {|qualifier| 
      name = qualifier.getName
      type_name = qualified_name(qualifier.getType)
      (Warnings << "Consider using type xsd:ID instead of #{type_name} for qualifier #{name}") if ('xsd:ID' != type_name) && !id
      if ('xsd:ID' == type_name) && (id.getID != qualifier.getID)
        type_name = 'xsd:string'
        Warnings << "XML Schema only allows one attribute derived from type xsd:ID - qualifier #{id.getName} is being used as an id, so an xsd:string will be used for qualifier #{name}"
      end
      insert_xsd_with_documentation(qualifier, :attribute, :name=>"qualifier_#{name}", :type=>type_name, :use=>'required') 
      }
    _build_attributes_for_cls(cls)
  end
  def _build_implementers(interface)
# puts "NOT extendable"
      # InterfaceRealization instances sort on the basis of the implementing classes (for reproducability).
      realizations = interface.get_interfaceRealizationOfContract.to_array.sort
      realizations.sort.each {|realization|
        classifier = realization.getImplementingClassifier
# puts "\t+#{classifier.getName}"
        next unless is_available?(classifier.get_uri)
        unless classifier
            Warnings << "Realization of interface #{impl.getQualifiedName} is missing ImplementingClassifier" 
            next
        end
        # E18 PVType insists on having multiple choices for the same type (for example xsd:int can have an element name of either I4 or EI). 
        # The element name is adding some additonal semantics to data (this should probably be done with a subclass xsd:int instead)
        # Nevertheless, we are stuck with it for the time being. It will probably mess up the import and the Pippin unmarshalling.
        eNames = (realization.get_tag_value(:Element, :names) || classifier.getName).split(',').collect {|name| name.strip}
        eNames.sort.each {|eName|
          b.xsd(:element, :name=>eName, :type=>qualified_name(classifier))
          }
        }
# puts "\t======"
  end
  
  def _build_attributes_for_cls(cls)
      cls_has_simple_content = cls.getOwnedAttribute.any?{|a| a.get_stereotype('simpleContent')}
      sorted_attributes = 
      sorted_attributes(cls).each {|attribute| 
        # (Warnings << "Class #{cls.getName} unexpectedly had OwnedAttribute of class #{attribute.class.name}"; next) unless attribute.property? # Should never be triggered
        type = attribute.getType
        (Warnings << "Class #{cls.getName} OwnedAttribute #{attribute.getName} has no type. Ignoring the attribute."; next) unless type
        case
        when attribute.getUpper != 1
          Warnings << "Class #{cls.getName} OwnedAttribute #{attribute.getName} is multiple, but this is not allowed on a class that has <<simpleContent>> attributes. Ignoring the attribute." if cls_has_simple_content
          # Only attributes with a maximum multiplicity of 1 can be handled as XML attributes.
        when !cls_has_simple_content && (attribute.get_stereotype('xmlElement') || (@generate_attributes_as_elements && attribute.get_stereotype('xmlAttribute').nil?))
          # Attributes marked as <<xmlElements>> are treated as elements and not attributes.
          # Attributes are also treated as elements if generate_attributes_as_elements is set to true, and the attribute was not marked with <<xmlAttribute>>.
        when type.primitive?
          _build_primitive_attribute(attribute)
        when type.enumeration?
          _build_enumeration_attribute(attribute)
        when type.interface?
          # An association
        when type.class?
          # An association
        else
          Warnings << "Class #{cls.getName} OwnedAttribute #{attribute.getName} unexpectedly had attribute type #{type.class.name}"
        end
        }
  end
  def _build_associations_for_cls(cls)
      cls_has_simple_content = cls.getOwnedAttribute.any?{|a| a.get_stereotype('simpleContent')}
      sorted_attributes(cls).each {|attribute| 
        # (Warnings << "Class #{cls.getName} unexpectedly had OwnedAttribute of class #{attribute.class.name}"; next) unless attribute.property? # Should never be triggered
        type = attribute.getType
        (Warnings << "Class #{cls.getName} OwnedAttribute #{attribute.getName} has no type. Ignoring the attribute."; next) unless type
        case
        # Handle primitives and enumerations with multiplicities > 1, tagged with <<xmlElement>>, or not tagged with <<xmlAttribute>> when @generate_attributes_as_elements is true 
        #   unless the class has a <<simpleContent>> attribute.
        # Other attributes will be handled by _build_attributes_for_cls        
        when (type.primitive? || type.enumeration?) && !cls_has_simple_content && ((attribute.getUpper != 1) || attribute.get_stereotype('xmlElement') || (@generate_attributes_as_elements && attribute.get_stereotype('xmlAttribute').nil?))
          type_name = qualified_name(attribute.getType)
          _build_association_from_attribute(attribute, type_name)
        when type.primitive?
          # Singular attribute
        when type.enumeration?
          # Singular attribute
        when attribute.isAssociationClass?
          _build_association_to_assocClass(attribute)
        when type.interface?
          _build_association_to_interface(attribute)
        when type.class?
          _build_association_to_class(attribute)
        else
          Warnings << "Class #{cls.getName} OwnedAttribute #{attribute.getName} unexpectedly had element type #{type.class.name}"
        end
        }
      return unless cls.isAssociationClass?
      # puts ">>>>>>>> assoc class #{cls.getName}"
      cls.getMemberEnd.each {|prop| 
        # puts ">>>>>>>> member end #{prop.getName} on #{prop.getOwnerClass.getName} pointing to #{prop.getType.getName}"
        mp = prop.mirror_property
        # puts ">>>>>>>> nav? #{prop.navigable?}   member? #{kept?(prop).inspect}  mp? #{(nil!=mp).inspect}"
        next unless prop.navigable?
        # puts ">>>>>>>> proceeding"
        type = prop.getType
        type_name = kept_ac?(prop) ? type.getName : 'xsd:ID'
        case
        # Handle primitives and enumerations with multiplicities > 1, tagged with <<xmlElement>>, or not tagged with <<xmlAttribute>> when @generate_attributes_as_elements is true 
        #   unless the class has a <<simpleContent>> attribute.
        # Other attributes will be handled by _build_attributes_for_cls
        when (type.primitive? || type.enumeration?) && !cls_has_simple_content && ((attribute.getUpper != 1) || attribute.get_stereotype('xmlElement') || (@generate_attributes_as_elements && attribute.get_stereotype('xmlAttribute').nil?))
          type_name = qualified_name(attribute.getType)
          _build_association_from_attribute(attribute, type_name)
        when type.primitive?
          # Singular attribute
        when type.enumeration?
          # Singular attribute
        when type.interface?
          _build_association_to_interface(prop, true, false) if kept_ac?(prop)
        when type.class?
          _build_association_to_classNamed(prop, type_name, false)
        else
          Warnings << "AssociationClass #{cls.getName} member end #{prop.getName} unexpectedly had getType of class #{type.class.name}"
        end
      } 
  end
  # This is the original version that creates an anonymous class as a place to hold the role name.
  def _build_association_to_interface_anonymous_class(attribute, fallback=true, allow_assoc_name=true)
    name = _get_role_name(attribute, fallback, allow_assoc_name)
    min = attribute.getLower
    max = attribute.getUpper
    max = 'unbounded' if max < 1
    interface = attribute.getType
    type_name = qualified_name(interface)
    # We could put the multiplicity on either the role or the group. 
    # * It's a bit more consistent to put it on the role
    # * Instances will be a bit more compact if it's put on the group.
    # If optional_multiplicity_on_group_element is true, make containing element optional if appropriate
    element_min = @optional_multiplicity_on_group_element && (min == 0) ? 0 : nil
    b.xsd(:element, :name=>name, :minOccurs => element_min) { 
      b.xsd(:complexType) {
        b.xsd(:sequence) {
              if interface.__is_extendable?
                b.xsd(:element, :ref=>type_name, :minOccurs=>min, :maxOccurs=>max)
              else
                b.xsd(:group, :ref=>type_name, :minOccurs=>min, :maxOccurs=>max) 
              end
            }
          }
      }
  end    
  # This newer version uses an annotation to hold the role name.
  # This is nice and compact, but you probably can't have (within the same class) multiple associations to the same interface
  def _build_association_to_interface_annotation(attribute, fallback=true, allow_assoc_name=true)
    name = _get_role_name(attribute, fallback, allow_assoc_name)
    min = attribute.getLower
    max = attribute.getUpper
    max = 'unbounded' if max < 1
    interface = attribute.getType
    type_name = qualified_name(interface)
    element_type = interface.__is_extendable? ? :element : :group
    b.xsd(element_type, :ref=>type_name, :minOccurs=>min, :maxOccurs=>max) {
      if name
        b.xsd(:annotation) {
          b.xsd(:appinfo) {
            b.pc(:roog, :roleName=>name)
            } 
          }
      end
    }
  end
  def _build_association_to_interface_annotation_lax(attribute, fallback=false, allow_assoc_name=true)
    _build_association_to_interface_annotation(attribute, fallback, allow_assoc_name)
  end
  def _build_association_to_interface(attribute, fallback=true, allow_assoc_name=true)
    send "_build_association_to_interface_#{$ASSOC_TO_IF_METHOD}", attribute, fallback, allow_assoc_name
  end
  def _build_association_to_assocClass(attribute)
    _build_association_to_classNamed(attribute, attribute.getAssociation.getName)
  end
  def _build_association_to_class(attribute, allow_assoc_name=true)
    _build_association_to_classNamed(attribute, qualified_name(attribute.getType), allow_assoc_name)
  end
  def _build_association_from_attribute(attribute, typeName)
    _build_association_to_classNamed(attribute, typeName, false, false, false)
  end
  def _build_association_to_classNamed(attribute, typeName, allow_assoc_name=true, allow_reference=true, required_if_unspecified=true)
    name = _get_role_name(attribute, true, allow_assoc_name)
    min = attribute.getLower
    # Setting required_if_unspecified=false specifies that the lower bound should be 0
    # if not explicitly set in model
    min = 0 if ''==ModelHelper.getMultiplicity(attribute) && !required_if_unspecified
    max = attribute.getUpper
    max = 'unbounded' if max < 1
    # Determine whether this is an attribute (as opposed to a normal association)
    # This is important since we consider attribute-defined associations to be composition
    is_attribute = !attribute.association
    is_composite = attribute.isComposite || is_attribute
    # This is a modification to the results of the Edmonds algorithm
    # to always keep composition associations (as desired for Voting model)

    # keep_element = !allow_reference || kept?(attribute) || (always_keep_compositions && (attribute.isComposite || is_attribute))
    keep_element = case 
      # Setting allow_reference=false specifies that this association should always be 'kept'
      when allow_reference == false
        true
      # Ignore Edmonds algorithm results and only rely on composition status to determine if association is 'kept'
      when only_keep_compositions
        is_composite
      # This is a modification to the results of the Edmonds algorithm
      # to always keep composition associations (as desired for Voting model)  
      when always_keep_compositions && is_composite
        true
      else
        kept?(attribute)
      end
    
    case
    when 'Gui_Builder_Profile:RichText'==typeName
      type_name = 'xsd:string'
    when keep_element
      type_name = typeName
      type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
      # Check for usage of MD primitives which should be mapped to XSD types
      xsd_type = get_xsd_type_for_attribute(attribute)
      type_name = xsd_type if xsd_type
    else # This is a referenced element. Type is xsd:IDREF (or xsd:IDREFS), and optionally append 'ID(s)' to name
      using_idrefs = @use_idrefs && (max == 'unbounded' || max > 1)
      if using_idrefs
        type_name = 'xsd:IDREFS'
        # Change max to 1 since a single IDREFS will contain all references
        max = 1
        name = name + @id_suffix_for_plural_references if @append_id_to_reference_names
      else
        type_name = 'xsd:IDREF'
        name = name + @id_suffix_for_references if @append_id_to_reference_names
      end
    end
    
    if attribute.get_stereotype('referenceElement')
      attrs = {:ref=>type_name}
    else
      attrs = {:name=>name, :type=>type_name}
      attrs[:name] = capitalize_first_letter(attrs[:name]) if @enforce_case_conventions
    end
    attrs[:minOccurs]=min unless 1==min
    attrs[:maxOccurs]=max unless 1==max
    
    if use_collection_containers && max != 1
      # Append "Collection" to association name to form container name
      container_name = attrs[:name] + 'Collection'
      container_attrs = {:name => container_name, :minOccurs => attrs[:minOccurs]}
      b.xsd(:element, container_attrs) {
        b.xsd(:complexType) {
          b.xsd(:sequence) {
            insert_xsd_with_documentation(attribute, :element, attrs.merge(:minOccurs => 1))
          }
        }
      }
    else
      insert_xsd_with_documentation(attribute, :element, attrs)
    end
  end
  def _build_attribute(attribute)
    # Do not create an XML attribute for <<simpleContent>> attributes.
    # They are handled as the content of the current element.
    return if attribute.get_stereotype('simpleContent')
    
    Warnings << "Ignored multiple values of attribute #{attribute.getName}" if attribute.getUpper > 1
    type_name = qualified_name(attribute.getType)
    type_name = type_name + 'Type' if @append_type_to_type_names && !(type_name =~ /^xsd:/)
    # Check for usage of MD primitives which should be mapped to XSD types
    xsd_type = get_xsd_type_for_attribute(attribute)
    type_name = xsd_type if xsd_type
    name = attribute.getName
    name = (@uppercase_attribute_names ? capitalize_first_letter(name) : decapitalize(name)) if @enforce_case_conventions
    
    attribute_attrs = {:name => name, :type => type_name}    
    # Attribute has a maximum multiplicity of 1. Attributes with a higher multiplicity are handled as associations
    unless 0==attribute.getLower || ''==ModelHelper.getMultiplicity(attribute) # per Eric's request, regard unspecified multiplicity on attribute as optional
      attribute_attrs[:use] = 'required'
    end
    insert_xsd_with_documentation(attribute, :attribute, attribute_attrs)
  end
  alias _build_primitive_attribute   _build_attribute
  alias _build_enumeration_attribute _build_attribute
  
  # Return an XSD type for an attribute's type if appropriate (e.g. UML String would map to xsd:string)
  def get_xsd_type_for_attribute(attribute)
    return nil unless attribute.getType.primitive?
    case
    when attribute.get_stereotype('xsd_language')
      xsd_type = 'xsd:language'
    # Add stereotypes for other xsd types here as needed. See XsdMetamodel.mdzip in umm_xml_schema for a helpful diagram of other XSD datatypes. -SD
    else
      xsd_type = get_xsd_type_for_type(attribute.getType)
    end
  end
  
  def get_xsd_type_for_type(type)
    xsd_type = md_xsd_type_mapping[type]
    'xsd:' + xsd_type if xsd_type
  end
  
  def md_xsd_type_mapping
    md_umlp = MagicDraw.uml_primitives
    md_mdpd = MagicDraw.md_profile_datatypes
    mda_p = MagicDrawAuxiliary.primitives
    
    {
      # Standard UML primitive mappings
      md_umlp[:integer] => 'integer',
      md_umlp[:real] => 'decimal',
      md_umlp[:string] => 'string',
      md_umlp[:boolean] => 'boolean',
      md_umlp[:unlimitednatural] => 'integer',
      
      # MD Profile datatype mappings
      md_mdpd[:int] => 'int',
      md_mdpd[:long] => 'long',
      md_mdpd[:short] => 'short',
      md_mdpd[:byte] => 'base64Binary',
      md_mdpd[:char] => 'string',
      md_mdpd[:date] => 'date',
      md_mdpd[:float] => 'float',
      md_mdpd[:double] => 'double',
      md_mdpd[:boolean] => 'boolean',
      md_mdpd[:void] => nil, # No sensible mapping available
      
      # MD Auxiliary primitive mappings
      mda_p[:datetime] => 'dateTime',
      mda_p[:regularexpression] => 'string',
      mda_p[:time] => 'time',
      mda_p[:uri] => 'anyURI'
    }
  end
  
  # Converts the first capital letter (and any directly following capital letters) to lowercase
  # Examples:
  #   FooBar -> fooBar
  #   URL -> url
  #   PDFFileContents -> pdffileContents
  def decapitalize(name)
    name.gsub(/^[A-Z]+/) {|m| m.downcase}
  end
  
  def capitalize_first_letter(string)
    return string unless string && !string.empty?
    string[0].upcase + string[1..-1]
  end
  
  def xml_super(object)
    if object.is_enumeration?
      return enum_xml_super(object)
    else
      return element_xml_super(object)
    end
  end
  
  def element_xml_super(element)
    superClasses = element.getGeneral
    case
        when superClasses.size < 1
          Warnings << "#{element.category_name} #{element.getName} does not have a superClass: presuming xsd:string"
          'xsd:string'
        when superClasses.size == 1
          get_xsd_type_for_type(superClasses[0]) || qualified_name(superClasses[0])
        else # superClasses.size > 1
          name = superClasses[0].getName
          Warnings << "#{element.category_name} #{element.getName} has multiple superClasses: presuming #{name}"
          get_xsd_type_for_type(superClasses[0]) || qualified_name(superClasses[0])
    end
  end
  
  def enum_xml_super(enum)
    type_attribute = enum.getOwnedAttribute.to_array.select {|attr| 'type'==attr.getName}[0]
    base = nil
    case
      when nil==type_attribute         #; Warnings << "Enumeration #{enum.getName} should have a 'type' attribute: presuming xsd:string"
      when nil==type_attribute.getType #; Warnings << "Enumeration #{enum.getName} has a 'type' attribute, but its 'Type' is not specified: presuming xsd:string"
      else base = get_xsd_type_for_attribute(type_attribute) || qualified_name(type_attribute.getType)
    end
    base = 'xsd:string' unless base
    base
  end

  def schema_fileName(package)
    unless @schema_filename.empty?
      @schema_filename
    else
      package.getName+'.xsd'
    end
  end
  def schema_filePath(package); File.join(Generator.file_root, schema_fileName(package)); end
  
  def build(root_package)
    root_model_package = UMLPackage.new(root_package)
    setup(root_model_package)
    build_package(root_package)
    xml
  end
  
  # Inserts a given xsd element, conditionally inserting documentation annotation 
  def insert_xsd_with_documentation(property, *args, &block)
    documentation = property.documentation
    if @include_documentation_annotations && documentation && !(documentation =~ /\A\s*\Z/)
      b.xsd(*args) {
        b.xsd(:annotation) {
          doc = fix_quotes_strip_html_and_whitespace(documentation)
          puts "Got doc: \n#{doc}\n"
          puts doc.inspect
          if @documentation_inline
            b.xsd(:documentation, doc, :'xml:lang' => 'en')
          else
            b.xsd(:documentation, :'xml:lang' => 'en') { 
              if @indent_documentation
                b.indented_multiline_text!(doc)
              else
                b.escaped_stripped_text!(doc)
              end
            }
          end
        }
        yield if block_given?
      }
    else
      b.xsd(*args, &block)
    end
  end
  
  # Handle badly formatted MagicDraw HTML documentation, stripping it of HTML tags and excessive whitespace
  def fix_quotes_strip_html_and_whitespace(text)
    # Replace non breaking spaces with regular space
    text.gsub!(/(&nbsp;|&#160;)/, ' ')
    # Fix quotes
    text.gsub!(/&rsquo;/, '\'')
    text.gsub!(/&lsquo;/, '\'')
    text.gsub!(/&rdquo;/, '"')
    text.gsub!(/&ldquo;/, '"')
    # Remove HTML tags
    text.gsub!(/<[^>]*>/, '')
    # Remove leading and trailing whitespace for entire string
    text.strip!
    # Remove trailing whitespace for individual lines
    text.gsub!(/[ \t]+$/, '')
    # Remove multiple sequential empty lines
    lines = text.split("\n")
    lines.each_with_index{|line, i| lines[i] = nil if (lines[i].empty? && lines[i+1].empty?)}
    lines.compact!
    text = lines.join("\n")
  end
      
  # Return sorted list of attributes
  def sorted_attributes(cls)
    # Sort alphanumerically, ignoring case
    sorted = cls.getOwnedAttribute.to_array.sort_by{|a| sortable_attribute_name(a).downcase }
    # Sort Other<attribute> and End<attribute> matching attributes
    # First find matching pairs
    sorted_names = sorted.collect{|a| sortable_attribute_name(a)}
    paired_attrs = []
    sorted.each do |a|
      name = sortable_attribute_name(a)
      if @sort_other_attributes_following_base_attributes
        other_attr = sorted.find{|a| sortable_attribute_name(a) == "Other#{name}"}
        paired_attrs << [a, other_attr] if other_attr
      end
      if @sort_end_attributes_following_start_attributes && name =~ /^Start(.*)$/i
        end_attr = sorted.find{|a| sortable_attribute_name(a) == "End#{$1}"}
        paired_attrs << [a, end_attr] if end_attr
      end
      if @sort_end_attributes_following_start_attributes && name =~ /^(.*)Start/i
        end_attr = sorted.find{|a| sortable_attribute_name(a) == "#{$1}End"}
        paired_attrs << [a, end_attr] if end_attr
      end
    end
    # Now reorder paired_attrs
    paired_attrs.each do |a, paired_attr|
      value = sorted.delete(paired_attr)
      new_index = sorted.index(a) + 1
      sorted.insert(new_index, paired_attr)
    end
    sorted
  end
  
  # Get a name for an attribute suitable for sorting
  # Returns type name if no attribute name is present
  # Returns empty string if no attribute name or type is present
  def sortable_attribute_name(attribute)
    name = attribute.getName
    type = attribute.getType
    name.empty? ? (type ? type.getName : '') : name
  end
end
