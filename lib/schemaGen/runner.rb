# SchemaGen plugin for MagicDraw: generates XML Schemas from UML class models.
# Copyright (C) 2007  Prometheus Computing LLC
# See license.txt

# The following is required by non-schema code which was inappropriately thrown into schemaGen
# require 'schemaGen/initialize'

load 'magicdraw_extensions.rb'

# NOTE:  if reproduceable_builder, xml_builder, and uml_models are switched from 'require' to 'load' a stack overflow occurs on the second execution of xml schema generation.
#        The resulting stack dump is inconsistent, often does not show much depth, and tends to occur in innocuous code.
#        This problem may have been fixed by change to modelDriven_builder.rb
require 'schemaGen/xml_builder'
# load 'javaGen.rb'   # Not part of schema generation
# load 'rubyGen.rb'   # Not part of schema generation
load 'modelDriven_builder/uml_models.rb'


$MODEL_TYPE = :STEREOTYPE

# Desirable features not yet implemented:
  # Allow non-attribute Strings
  # Generalization of interfaces (need to worry about mixing two kinds of interfaces?)
  # Configuration settings
  # Ternary and higher associations
  # Compositon, Aggregation diamonds
  # Additional features from paper
  # Make xsd profile complete
  # Could provide more warnings
    # Realization of multiple extendable interfaces
    # Root that implements an extendable interface

# Now supported by XML Schema generator:
    # Association classes
    # Qualifiers  
    # Non-tree graphs

  
# =================================================================================

# Specifies how association to an interface is represented in the schema.
# The problem is how to squeeze in both the role name and the Interface name.
# To save repetition, in the following, "gdeg" means: "globally defined element (subsitution group head) or group (containing only a choice)"
# Valid values are:
# :annotation -  The interface name is specified by ref to a gdeg containing an annotation specifying the role name.
#               Should not use if there are multiple associations from a given class to the same interface (because this might result in a
#               non-deterministic schema)
# :annotation_lax - Like :annotation, except the child annotation element is not provided unless the role name is explicitly indicated in the model.
# :anonymous_class - The role name is the name of an element with an anonymous complexType (class) containing only one ref to a gdeg.
# :combine - don't recall what this was (it's not implemented)
$ASSOC_TO_IF_METHOD = :anonymous_class
$ROLE_TYPE_SEP = '___'

# =================================================================================

#generator = Generator.instance
#generator.package = $SELECTION
#schemaGen_action = $Action
case $Action
when :generate_xmlSchema
  XMLBuilder.generate_schema($SELECTION)
  
  # builder = XMLBuilder.new
  #
  # folder_name = generator.main_module_name.downcase
  #
  # generation_folder = File.join(Generator.file_root, folder_name)
  # puts "Generation folder: #{generation_folder}"
  # puts '?' * 50
  # results = []
  #
  # # Generate the reference implementation (run schemaGen)
  # generator.builder = builder
  # generator_result = generator.build
  # results << generator_result unless generator_result =~ /Success, no warnings!/
  # $MESSAGE = results.empty? ? "Success, no warnings!" : results.join("\n")
else
  puts "INVALID ACTION: #{$Action.inspect}"
end
